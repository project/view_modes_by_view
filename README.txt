README.txt
==========

View modes by View provides a report that shows you which views are using which
view modes.

INSTALLATION
============

There are no special installation steps. Install it like any other module. To
wit:

1. Place the entirety of this directory in sites/all/modules/view_modes_by_view.
2. Navigate to Administer >> Build >> Modules.
3. Enable View modes by View.

USAGE
=====

After installing the module, navigate to admin/reports/view-modes/views. The
report is set up as a table and very closely resembles the Field List.

Note: You must have the 'administer views' permission to see the report.

The first column (View Mode) lists the each view mode by machine name.

The second column (Used In) lists all the views (and the views displays) in
which the view mode is used.

